﻿namespace Project_Structure.DAL.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
