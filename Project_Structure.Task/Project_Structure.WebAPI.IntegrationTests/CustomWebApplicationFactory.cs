﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.API;
using Project_Structure.DAL.Context;
using System.Linq;

namespace Project_Structure.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(ConfigureServices);
        }

        private void ConfigureServices(WebHostBuilderContext webHostBuilderContext, IServiceCollection serviceCollection)
        {
            var dbContextService = serviceCollection.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectDbContext>));
            if (dbContextService != null)
            {
                // remove the DbContext that is registered on StartUp.cs
                serviceCollection.Remove(dbContextService);
            }

            // register the new DbContext, .NET Core dependency injection framework will now use the this instance.
            serviceCollection.AddDbContext<ProjectDbContext>(contextOptions => contextOptions.UseInMemoryDatabase("ProjectDb"));

        }

    }
}
