﻿using Project_Structure.BLL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<IEnumerable<UserDTO>> GetAllUsers();
        public Task<UserDTO> GetUserById(int id);
        public Task<UserDTO> CreateUser(UserDTO user);
        public Task<UserDTO> UpdateUser(UserDTO user);
        public Task DeleteUser(int idTask);
    }
}
