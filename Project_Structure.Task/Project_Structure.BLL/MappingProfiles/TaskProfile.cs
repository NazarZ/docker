﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.DAL.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>().ReverseMap();
        }
    }
}
