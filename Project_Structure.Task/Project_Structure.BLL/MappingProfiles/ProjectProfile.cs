﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.DAL.Entities;

namespace Project_Structure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>().ReverseMap();
        }
    }
}
