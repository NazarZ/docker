﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public async Task<ProjectDTO> CreateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            await _context.Projects.Create(projectEntity);
            await _context.SaveAsync();
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
        public async System.Threading.Tasks.Task DeleteProject(int idProject)
        {
            await _context.Projects.Delete(idProject);
            await _context.SaveAsync();
        }
        public void Dispose()
        {

        }
        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(await _context.Projects.GetAll());
        }
        public async Task<ProjectDTO> GetProjectById(int id)
        {
            return _mapper.Map<ProjectDTO>(await _context.Projects.Get(id));
        }
        public async Task<ProjectDTO> UpdateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            await _context.Projects.Update(projectEntity);
            await _context.SaveAsync();
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
    }
}
