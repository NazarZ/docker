﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public async Task<UserDTO> CreateUser(UserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);
            await _context.Users.Create(userEntity);
            await _context.SaveAsync();
            return _mapper.Map<UserDTO>(userEntity);
        }

        public async System.Threading.Tasks.Task DeleteUser(int idItem)
        {
            await _context.Users.Delete(idItem);
            await _context.SaveAsync();
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(await _context.Users.GetAll());
        }

        public async Task<UserDTO> GetUserById(int idItem)
        {
            return _mapper.Map<UserDTO>(await _context.Users.Get(idItem));
        }

        public async Task<UserDTO> UpdateUser(UserDTO item)
        {
            var userEntity = _mapper.Map<User>(item);
            await _context.Users.Update(userEntity);
            await _context.SaveAsync();
            return _mapper.Map<UserDTO>(userEntity);
        }
    }
}
