﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTOs
{
    public class TeamDTO
    {

        public int Id { get; set; }
        [MaxLength(200)]
        public string? Name { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}
