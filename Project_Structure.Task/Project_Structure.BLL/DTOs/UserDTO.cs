﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int? TeamId { get; set; }
        [MaxLength(20)]
        public string? FirstName { get; set; }
        [MaxLength(20)]
        public string? LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string? Email { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }


        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            UserDTO otherUserDTO = obj as UserDTO;
            if (otherUserDTO != null)
            {
                return this.Id.Equals(otherUserDTO.Id);
            }
            else
            {
                throw new ArgumentException("Type of object isn't valid");
            }
        }

    }
}
