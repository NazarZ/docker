﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.BLL.DTOs
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int AuthorId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The field {0} must be greater than {1}.")]
        public int TeamId { get; set; }
        [MaxLength(200)]
        public string? Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }
    }
}
